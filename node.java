/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

import java.util.ArrayList;

public class node {

	private String name;
  private Integer numAristas;
  private Integer index;
  /*Estas variables solo son usadas en el modelo geográfico simple
  por lo que solo se usan cuando se llama al constructor correspondiente*/
  private double x;
  private double y;

  /*Constructor que toma una cadena como nombre del vértice*/
  public node(String name) {
    this.name = name;
    this.numAristas = 0;
  }

  /*Constructor que toma un entero como argumento. Asigna a la variable
  de instancia 'name' la cadena formada por la letra 'n' concatenada con
  la representación en cadena del número entero que tomó como argumento*/
  public node(int name) {
    this.index = name;
    this.name = "n" + String.valueOf(name);
    this.numAristas = 0;
  }

  public node(int name, double x, double y) {
    this.index = name;
    this.name = "n" + String.valueOf(name);
    this.x = x;
    this.y = y;
  }

  /*getters de variables de instancia */

  public String getName() {return name;}

  public Integer getNumEdges() {return numAristas;}

  public Integer getIndex() {return index;}
  public double getX() {return x;}

  public double getY() {return y;}
}


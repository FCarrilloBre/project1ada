
package graph;

import graph.node;
import graph.Arista;
import java.util.Scanner;
import java.util.*;
import java.util.Formatter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedWriter;
import java.util.FormatterClosedException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

public class GraphBA {
    private int L;
    private node[] vert;
    private HashMap<node, HashSet<node>> nodos;
    private final int numeroVertices;
    private int numeroAristas=0;
    private int num_nodos; //número de vértices del grafo
    private int num_aristas;  //número de aristas únicas del grafo
    private static Formatter output; //objeto para escribir a disco
   
    public int gradoVertice(int i) {
    node n1 = this.getNode(i);
    return this.nodos.get(n1).size();
  }    

    
    public node getNode(int i) {return this.vert[i];}
    public int getNumNodes() {return num_nodos;}
    public int getNumEdges() {return numeroAristas;}
    
    public HashSet<node> getEdges(int i) {
    node n = this.getNode(i);
    return this.nodos.get(n);
     }
    public void conectarVertices(int i, int j,boolean dirigido) {
    /*Se recuperan los vértices de los índices i y j*/
     node n1 = this.getNode(i);
     node n2 = this.getNode(j);
     
     HashSet<node> aristas1 = this.getEdges(i);
     HashSet<node> aristas2 = this.getEdges(j);

     if(dirigido==true){
         aristas1.add(n2);
         this.numeroAristas=numeroAristas+1;
     }
     else{
     aristas1.add(n2);
     aristas2.add(n1);  
     this.numeroAristas +=1;
     }
  }
    
    private Boolean existeConexion(int i, int j) {
    /*Se recuperan los vértices de los índices i y j*/
    node n1 = this.getNode(i);
    node n2 = this.getNode(j);
    /*Se recuperan las aristas de cada vértice*/
    HashSet<node> aristas1 = this.getEdges(i);
    HashSet<node> aristas2 = this.getEdges(j);
    /*Se revisa que un nodo esté en el conjunto de aristas del otro*/
     if (aristas1.contains(n2) || aristas2.contains(n1)) {
       return true;
     }
     else{
       return false;
     }
  }
    public GraphBA (int num_nodos, int num_d,boolean dirigido, String name)
	{
	       this.nodos = new HashMap<node, HashSet<node>>();
            this.numeroVertices = num_nodos;
            this.vert = new node[num_nodos];
            for (int i = 0; i < num_nodos; i++) {
                node n = new node(i);
                this.vert[i] = n;
                System.out.println(vert);
                this.nodos.put(n, new HashSet<node>());
                System.out.println(i);
      
            }
            if (dirigido==true){
                Random aleatorio = new Random();
                for(int i = 0; i < num_d; i++){
                    for(int j = 0; j < i; j++) {
                        if (!existeConexion(i, j)) {
                            conectarVertices(i, j,true);
                        }
                    } 
                 }
  
                for(int i = num_d; i <num_nodos;) {
                     for(int j = 0; j < i; j++) {
                        double probabilidad =(double)gradoVertice(j)/(double)this.getNumEdges();
                            if (aleatorio.nextDouble() <= probabilidad) {
                                if (!existeConexion(i, j) && (gradoVertice(i) < num_d)) {
                                    conectarVertices(i, j,true);
                                 }
                            }   
                    }
                    if (gradoVertice(i) >= num_d) i++;
                }
                String salida;
                salida ="graph {\n";
                for (int i = 0; i < num_nodos; i++) {
                    salida += this.getNode(i).getName() + ";\n";
       
        
                }
                for (int i = 0; i < num_nodos; i++) {
                    HashSet<node> aristas = this.getEdges(i);
         
                    for (node n : aristas) {
                        salida += this.getNode(i).getName() + " -- " + n.getName() + ";\n";
                    }
                }
                salida += "}\n";
                System.out.println(salida);
                try{
                FileWriter fw= new FileWriter(name);
                BufferedWriter bw=new BufferedWriter(fw);
                bw.write(salida);
                bw.close();
                }
                catch(IOException e){
                    System.out.print("ERROR");
                    System.exit(1);
                }    
             }
            else if(dirigido==false){
                Random aleatorio = new Random();
                for(int i = 0; i < num_d; i++){
                    for(int j = 0; j < i; j++) {
                        if (!existeConexion(i, j)) {
                            conectarVertices(i, j,false);
                        }
                    } 
                 }
  
                for(int i = num_d; i <num_nodos;) {
                     for(int j = 0; j < i; j++) {
                        double probabilidad =(double)gradoVertice(j)/(double)this.getNumEdges();
                            if (aleatorio.nextDouble() <= probabilidad) {
                                if (!existeConexion(i, j) && (gradoVertice(i) < num_d)) {
                                    conectarVertices(i, j,false);
                                 }
                            }   
                    }
                    if (gradoVertice(i) >= num_d) i++;
                }
                String salida;
                salida ="graph {\n";
                for (int i = 0; i < num_nodos; i++) {
                    salida += this.getNode(i).getName() + ";\n";
       
        
                }
                for (int i = 0; i < num_nodos; i++) {
                    HashSet<node> aristas = this.getEdges(i);
         
                    for (node n : aristas) {
                        salida += this.getNode(i).getName() + " -- " + n.getName() + ";\n";
                    }
                }
                salida += "}\n";
                System.out.println(salida);
                try{
                FileWriter fw= new FileWriter(name);
                BufferedWriter bw=new BufferedWriter(fw);
                bw.write(salida);
                bw.close();
                }
                catch(IOException e){
                    System.out.print("ERROR");
                    System.exit(1);
                }    
            }
        }
               
                
                
}

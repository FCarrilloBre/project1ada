
package graph;


public class Arista {
  /* Variables de instancia*/
  private Integer n1;
  private Integer n2;
  private double peso;


  public Arista(int n1, int n2) {
    this.n1 = n1;
    this.n2 = n2;
  }

  /*getters y de variables de instancia */

  public String getNode1() {return "n" + n1.toString();}

  public String getNode2() {return "n" + n2.toString();}

  public int getIntN1() {return n1;}

  public int getIntN2() {return n2;}

}



package graph;
import graph.GraphER;
import graph.GraphGil;
import graph.GraphGeo;
import graph.GraphBA;
import java.util.Scanner;
public class Proyecto {

    public static void main(String[] args) {
         Scanner sc = new Scanner(System.in);
        int modelo = 0;
        int n,m;
        Boolean Autci;
        

        while (modelo != 5) {
            System.out.println("Elige el modelo que quieres:");
            System.out.println("1.- Erdös-Renyi");
            System.out.println("2.- Modelo de Gilbert");
            System.out.println("3.- Modelo geografico simple");
            System.out.println("4.- Variante del modelo Barabasi Albert");
            System.out.println("5.- Salir");
            modelo = sc.nextInt();

            switch (modelo) {
                case 1:
                    System.out.println("Introduce el nombre de tu archivo .gv");
                    sc.nextLine();
                    String name = sc.nextLine();
                    
                    System.out.println("Introduce el número de nodos:");
                    
                    n = sc.nextInt();
                    System.out.println("Introduce el número de parejas posibles:");
                    m = sc.nextInt();
                    System.out.println("Introduce true si quieres que haya auotciclo");
                    System.out.println("De lo contratrio introduce false");
                    Autci = sc.nextBoolean();
                    System.out.println("Introduce true si quieres que sea dirigido");
                    System.out.println("De lo contratrio introduce false");
                    boolean dirig = sc.nextBoolean();
                    GraphER uno=new GraphER(n,m,Autci,dirig,name);
                    break;
                    
                case 2:
                    System.out.println("Introduce el nombre de tu archivo .gv");
                    sc.nextLine();
                    String name2 = sc.nextLine();
                    System.out.println("Introduce el número de nodos:");
                    n = sc.nextInt();
                    System.out.println("Introduce la probabilidad de que se formen aristas:");
                    double prob = sc.nextDouble();
                    System.out.println("Introduce true si quieres que haya auotciclo");
                    System.out.println("De lo contratrio introduce false");
                    Autci = sc.nextBoolean();
                    System.out.println("Introduce true si quieres que sea dirigido");
                    System.out.println("De lo contratrio introduce false");
                    boolean dirig2 = sc.nextBoolean();
                    GraphGil dos = new GraphGil(n,prob,Autci,dirig2,name2);
                    break;
                    
                case 3:
                    System.out.println("Introduce el nombre de tu archivo .gv");
                    sc.nextLine();
                    String name3 = sc.nextLine();
                    System.out.println("Introduce el número de nodos:");
                    n = sc.nextInt();
                    System.out.println("Introduce la distancia de separación:");
                    double dist = sc.nextDouble();
                    System.out.println("Introduce true si quieres que haya auotciclo");
                    System.out.println("De lo contratrio introduce false");
                    Autci = sc.nextBoolean();
                    System.out.println("Introduce true si quieres que sea dirigido");
                    System.out.println("De lo contratrio introduce false");
                    boolean dirig3 = sc.nextBoolean();
                    GraphGeo tres = new GraphGeo(n,dist,Autci,dirig3,name3);
                    break;
                case 4:
                    System.out.println("Introduce el nombre de tu archivo .gv");
                    sc.nextLine();
                    String name4 = sc.nextLine();
                    System.out.println("Introduce el número de nodos:");
                    n = sc.nextInt();
                    System.out.println("Introduce el número de aristas para vertices:");
                    m = sc.nextInt();
                    System.out.println("Introduce true si quieres que sea dirigido");
                    System.out.println("De lo contratrio introduce false");
                    boolean dirig4 = sc.nextBoolean();
                    GraphBA cuatr=new GraphBA(n,m,dirig4,name4);
                    break;
                
                default:
                    System.out.println("Tienes que introducir una opción valida");
            }
        }
        
    }
    
}

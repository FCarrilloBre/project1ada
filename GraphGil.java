package graph;
import graph.node;
import graph.Arista;
import java.io.BufferedWriter;
import java.util.Scanner;
import java.util.*;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author fcarr_000
 */
public class GraphGil {
    private int L;
    private node[] vert;
    private HashMap<node, HashSet<node>> nodos;
    private final int numeroVertices;
    private int numeroAristas=0;
    private int num_nodos; //número de vértices del grafo
    private int num_aristas;  //número de aristas únicas del grafo
    private static Formatter output; //objeto para escribir a disco
    
    public node getNode(int i) {return this.vert[i];}
    public int getNumNodes() {return num_nodos;}
    public int getNumEdges() {return numeroAristas;}
    
    public HashSet<node> getEdges(int i) {
    node n = this.getNode(i);
    return this.nodos.get(n);
     }
    public void conectarVertices(int i, int j,boolean dirigido) {
    /*Se recuperan los vértices de los índices i y j*/
     node n1 = this.getNode(i);
     node n2 = this.getNode(j);
     
     HashSet<node> aristas1 = this.getEdges(i);
     HashSet<node> aristas2 = this.getEdges(j);

     if(dirigido==true){
         aristas1.add(n2);
         this.numeroAristas=numeroAristas+1;
     }
     else{
     aristas1.add(n2);
     aristas2.add(n1);  
     this.numeroAristas +=1;
     }
  }
    public String toString() {
    String salida;
    
      salida ="graph {\n";
      for (int i = 0; i < num_nodos; i++) {
        salida += this.getNode(i).getName() + ";\n";
        System.out.println(i);
        
      }
      for (int i = 0; i < num_nodos; i++) {
        HashSet<node> aristas = this.getEdges(i);
        for (node n : aristas) {
        salida += this.getNode(i).getName() + " -- " + n.getName() + ";\n";
        }
       }
      salida += "}\n";
    
    return salida;
  }
    private Boolean existeConexion(int i, int j) {
    /*Se recuperan los vértices de los índices i y j*/
    node n1 = this.getNode(i);
    node n2 = this.getNode(j);
    /*Se recuperan las aristas de cada vértice*/
    HashSet<node> aristas1 = this.getEdges(i);
    HashSet<node> aristas2 = this.getEdges(j);
    /*Se revisa que un nodo esté en el conjunto de aristas del otro*/
     if (aristas1.contains(n2) || aristas2.contains(n1)) {
       return true;
     }
     else{
       return false;
     }
  }
    public GraphGil (int num_nodos, double probabilidad, boolean autociclo,boolean dirigido, String name)
	{
	       this.nodos = new HashMap<node, HashSet<node>>();
            this.numeroVertices = num_nodos;
            this.vert = new node[num_nodos];
            for (int i = 0; i < num_nodos; i++) {
                node n = new node(i);
                this.vert[i] = n;
                System.out.println(vert);
                this.nodos.put(n, new HashSet<node>());
                System.out.println(i);
      
            }
            if ((autociclo==false) && (dirigido==false)){
            Random randomNum = new Random();
             for(int i = 0; i < num_nodos; i++) {
                for(int j = 0; j <num_nodos; j++) {
                    if ((i != j) && (randomNum.nextDouble() <= probabilidad)) {
                        if (!existeConexion(i, j)) {
                                conectarVertices(i, j,false);
                        }
                    }
                }
            }
             String salida;
                salida ="graph {\n";
                for (int i = 0; i < num_nodos; i++) {
                    salida += this.getNode(i).getName() + ";\n";
       
        
                }
                for (int i = 0; i < num_nodos; i++) {
                    HashSet<node> aristas = this.getEdges(i);
         
                    for (node n : aristas) {
                        salida += this.getNode(i).getName() + " -- " + n.getName() + ";\n";
                    }
                }
                salida += "}\n";
                System.out.println(salida);
            try{
                FileWriter fw= new FileWriter(name);
                BufferedWriter bw=new BufferedWriter(fw);
                bw.write(salida);
                bw.close();
                }
                catch(IOException e){
                    System.out.print("ERROR");
                    System.exit(1);
                }    
            
  }         else if ((autociclo==false) && (dirigido==true)){
            Random randomNum = new Random();
             for(int i = 0; i < num_nodos; i++) {
                for(int j = 0; j <num_nodos; j++) {
                    if ((i != j) && (randomNum.nextDouble() <= probabilidad)) {
                        if (!existeConexion(i, j)) {
                                conectarVertices(i, j,true);
                        }
                    }
                }
            }
              String salida;
                salida ="graph {\n";
                for (int i = 0; i < num_nodos; i++) {
                    salida += this.getNode(i).getName() + ";\n";
       
        
                }
                for (int i = 0; i < num_nodos; i++) {
                    HashSet<node> aristas = this.getEdges(i);
         
                    for (node n : aristas) {
                        salida += this.getNode(i).getName() + " -> " + n.getName() + ";\n";
                    }
                }
                salida += "}\n";
                System.out.println(salida);
                try{
                FileWriter fw= new FileWriter(name);
                BufferedWriter bw=new BufferedWriter(fw);
                bw.write(salida);
                bw.close();
                }
                catch(IOException e){
                    System.out.print("ERROR");
                    System.exit(1);
                }    
            }
            else if ((autociclo==true) && (dirigido==false)){
            Random randomNum = new Random();
             for(int i = 0; i < num_nodos; i++) {
                for(int j = 0; j <num_nodos; j++) {
                    if (randomNum.nextDouble() <= probabilidad) {
                        if (!existeConexion(i, j)) {
                                conectarVertices(i, j,false);
                        }
                    }
                }
            }
              String salida;
                salida ="graph {\n";
                for (int i = 0; i < num_nodos; i++) {
                    salida += this.getNode(i).getName() + ";\n";
       
        
                }
                for (int i = 0; i < num_nodos; i++) {
                    HashSet<node> aristas = this.getEdges(i);
         
                    for (node n : aristas) {
                        salida += this.getNode(i).getName() + " -- " + n.getName() + ";\n";
                    }
                }
                salida += "}\n";
                System.out.println(salida);
                try{
                FileWriter fw= new FileWriter(name);
                BufferedWriter bw=new BufferedWriter(fw);
                bw.write(salida);
                bw.close();
                }
                catch(IOException e){
                    System.out.print("ERROR");
                    System.exit(1);
                }    
            }
            else if ((autociclo==true) && (dirigido==true)){
            Random randomNum = new Random();
             for(int i = 0; i < num_nodos; i++) {
                for(int j = 0; j <num_nodos; j++) {
                    if (randomNum.nextDouble() <= probabilidad) {
                        if (!existeConexion(i, j)) {
                                conectarVertices(i, j,true);
                        }
                    }
                }
            }
              String salida;
                salida ="graph {\n";
                for (int i = 0; i < num_nodos; i++) {
                    salida += this.getNode(i).getName() + ";\n";
       
        
                }
                for (int i = 0; i < num_nodos; i++) {
                    HashSet<node> aristas = this.getEdges(i);
         
                    for (node n : aristas) {
                        salida += this.getNode(i).getName() + " -> " + n.getName() + ";\n";
                    }
                }
                salida += "}\n";
                System.out.println(salida);
                try{
                FileWriter fw= new FileWriter(name);
                BufferedWriter bw=new BufferedWriter(fw);
                bw.write(salida);
                bw.close();
                }
                catch(IOException e){
                    System.out.print("ERROR");
                    System.exit(1);
                }    
            }
        }
                
}
    
